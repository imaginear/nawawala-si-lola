﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HomeManager : MonoBehaviour
{
	public Button buttonChat;
    public GameObject chatNotif;
    public TextMeshProUGUI chatNotifCounter;

    [Space(10)]
    public Button buttonGallery;
    [Space(10)]
    public Button buttonCamera;
    [Space(10)]
    public Button buttonML;
    public Button buttonGenshin;

    [Space(10)]

    public Button buttonHome;
    public Button buttonBack, buttonTabs;
    public RawImage wallImage;


    private static HomeManager _instance;
    public static HomeManager Instance
	{
		get
		{
            if (_instance == null)
                _instance = FindObjectOfType<HomeManager>();

			return _instance;
		}
	}

    public void Awake()
    {
        buttonChat.onClick.AddListener(OpenChatApp);
        buttonGallery.onClick.AddListener(OpenGalleryApp);
        buttonCamera.onClick.AddListener(OpenCameraApp);

        buttonML.onClick.AddListener(OpenMLApp);
        buttonGenshin.onClick.AddListener(OpenGenshinApp);

        buttonHome.onClick.AddListener(HomePressed);
    }

    public void OpenChatApp()
    {
        ChatManager.Instance.Open();
    }

    public void SetChatNotif()
    {
        int i = ChatManager.Instance.unreadChats.Count;
        chatNotif.SetActive(i > 0);
        chatNotifCounter.text = i.ToString();

    }

    public void OpenGalleryApp()
    {
        GalleryManager.Instance.Open();
    }

    public void OpenCameraApp()
    {
        CameraManager.Instance.Open();
    }

    public void HomePressed()
    {
        AppBase.CloseCurrent();
    }

    public void OpenMLApp()
    {
        DumbApps.Instance.Open("Mobile Legacy");
    }
    public void OpenGenshinApp()
    {
        DumbApps.Instance.Open("Genshin Impulse");
    }

    public void SetWallBG(Texture2D bg)
    {
        wallImage.texture = bg;
    }

    public void DownloadAndSetWallBG(string url)
    {
        StartCoroutine(DownloadAndSetWallBGInternal(url));
    }
    IEnumerator DownloadAndSetWallBGInternal(string url)
    {
        using (WWW www = new WWW(url))
        {
            yield return www;
            SetWallBG(www.texture);
        }
    }
}
