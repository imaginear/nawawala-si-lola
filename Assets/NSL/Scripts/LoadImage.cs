﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class LoadImage : MonoBehaviour
{
    public string url = "";

    protected static Dictionary<string, Texture2D> downloaded = new Dictionary<string, Texture2D>();

    // Start is called before the first frame update
    IEnumerator Start()
    {
        if (downloaded.ContainsKey(url))
        {
            GetComponent<RawImage>().texture = downloaded[url];
            yield break;
        }

        using (WWW www = new WWW(url))
        {
            yield return www;
            GetComponent<RawImage>().texture = www.texture;
            downloaded.Add(url, www.texture);


        }
    }
}
