﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Linq;

public class MainDirector : MonoBehaviour
{
    public List<ScriptableObject> events;

    public string currentDate = "Jan 1";

    public Text topBarTime;

    public GameRaycaster gameRaycaster;

    public bool fastMode = false;

    private static MainDirector _instance;
    public static MainDirector Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<MainDirector>();

            return _instance;
        }
    }

    void Start()
    {
        //gameRaycaster.StartGame2(()=> { Debug.Log("yahoo!"); });

        StartCoroutine("PlayEvents");
        Debug.Log("Events = " + events.Count);
    }

    public ChatSender SenderWaitingForReply = ChatSender.NONE;
    public bool paused = false;

    IEnumerator PlayEvents()
    {

        if (fastMode)
        {
            yield return new WaitForSeconds(2);

        }
        else
        {
            yield return new WaitForSeconds(0.2f);
        }

        foreach (var e in events)
        {
            Debug.Log(e.name);

            if(e is ChatDataIn)
            {
                ProcessChatDataIn((ChatDataIn)e);
            }
            else if (e is ChatDataOut)
            {
                ProcessChatDataOut((ChatDataOut)e);
            }
            else if (e is ChatPhotoDataIn)
            {
                ProcessChatPhotoDataIn((ChatPhotoDataIn)e);
            }
            else if (e is ChatPhotoDataOut)
            {
                ProcessChatPhotoDataOut((ChatPhotoDataOut)e);
            }


            else if (e is LowBattData)
            {
                ProcessLowBattData((LowBattData)e);
            }
            else if (e is PopupData)
            {
                ProcessPopupData((PopupData)e);
            }

            else if (e is DelayData)
            {
                if(!fastMode)
                    yield return new WaitForSeconds(((DelayData)e).delay);
                else
                    yield return new WaitForSeconds(((DelayData)e).delay / 10);

            }




            else if(e is AddPhotoToGalleryData)
            {
                ProcessAddPhotoToGalleryData((AddPhotoToGalleryData)e);
            }
            else if(e is WaitPhotoViewData)
            {
                ProcessWaitForPhotoView((WaitPhotoViewData)e);
            }



            else if (e is SetWallBGData)
            {
                ProcessSetWallBG((SetWallBGData)e);
            }
            else if (e is SetCamBGData)
            {
                ProcessSetCamBG((SetCamBGData)e);
            }

            else if (e is WaitAppOpen)
            {
                ProcessWaitAppOpen((WaitAppOpen)e);
            }



            else if (e is PlayAudioData)
            {
                ProcessPlayAudioData((PlayAudioData)e);
            }

            else if (e is BGMData)
            {
                ProcessBGMData((BGMData)e);
            }

            else if (e is PlayGameData)
            {
                ProcessPlayGameData((PlayGameData)e);
            }

            while (SenderWaitingForReply != ChatSender.NONE || paused)
            {
                if (!fastMode)
                    yield return new WaitForSeconds(1);
                else
                    yield return new WaitForSeconds(0.1f);
            }

            if (!fastMode)
                yield return new WaitForSeconds(1);
            else
                yield return new WaitForSeconds(0.1f);
        }

       yield break;
    }

    void ProcessChatDataIn(ChatDataIn data)
    {
        ChatManager.Instance.Push(data);
        HomeManager.Instance.SetChatNotif();

        topBarTime.text = data.time;
        currentDate = data.date;
    }
    void ProcessChatDataOut(ChatDataOut data)
    {
        Debug.Log("Set Panel");


        SenderWaitingForReply = data.receiver;
        MessageOptionsPanel.Instance.SetPanel(
            data.message1,
            data.message2,
            () => { data.answer = 0; ChatManager.Instance.Push(data); SenderWaitingForReply = ChatSender.NONE; MessageOptionsPanel.Instance.ClosePanel(); },
            () => { data.answer = 1; ChatManager.Instance.Push(data); SenderWaitingForReply = ChatSender.NONE; MessageOptionsPanel.Instance.ClosePanel(); }
        );

        topBarTime.text = data.time;
        currentDate = data.date;


    }
    void ProcessChatPhotoDataIn(ChatPhotoDataIn data)
    {
        ChatManager.Instance.Push(data);
        HomeManager.Instance.SetChatNotif();

        topBarTime.text = data.time;
        currentDate = data.date;

    }
    void ProcessChatPhotoDataOut(ChatPhotoDataOut data)
    {
        ChatManager.Instance.Push(data);

        topBarTime.text = data.time;
        currentDate = data.date;

    }

    void ProcessLowBattData(LowBattData data)
    {
        paused = true;

        if (!data.charged)
        {
            LowBattManager.Instance.Show(data.message, () => {
                paused = false;
            });

            BatteryValue.Instance.LowBatt();
        }
        else
        {
            LowBattManager.Instance.Hide(() => {
                paused = false;
            });

            BatteryValue.Instance.FullBatt();
        }
        
    }

    void ProcessPopupData(PopupData data)
    {
        paused = true;
        PopUpManager.Instance.Show(data.message, () => {
            paused = false;
        });
    }

    void ProcessAddPhotoToGalleryData(AddPhotoToGalleryData data)
    {
        GalleryManager.Instance.DownloadAndAddPhotoToGallery(data.url, data.id);
    }

    string waitingForPhotoView;
    void ProcessWaitForPhotoView(WaitPhotoViewData data)
    {
        paused = true;
        waitingForPhotoView = data.id;
        GalleryManager.OnPhotoViewedInGallery += OnPhotoViewed;
    }

    void OnPhotoViewed(string id)
    {
        if(id == waitingForPhotoView)
        {
            paused = false;
            SFXPlayer.PlayOneShot("clue alert");
            GalleryManager.OnPhotoViewedInGallery -= OnPhotoViewed;
            waitingForPhotoView = "";
        }
    }

    void ProcessSetWallBG(SetWallBGData data)
    {
        HomeManager.Instance.DownloadAndSetWallBG(data.url);
    }
    void ProcessSetCamBG(SetCamBGData data)
    {
        CameraManager.Instance.DownloadAndSetCamBG(data.url);
    }


    string waitingForAppOpen;
    void ProcessWaitAppOpen(WaitAppOpen data)
    {
        paused = true;
        waitingForAppOpen = data.appName;
        AppBase.OnAppOpened += OnAppOpened;
    }

    void OnAppOpened(string id)
    {
        if (id == waitingForAppOpen)
        {
            paused = false;
            SFXPlayer.PlayOneShot("clue alert");
            AppBase.OnAppOpened -= OnAppOpened;
            waitingForAppOpen = "";
        }
    }

    void ProcessPlayAudioData(PlayAudioData data)
    {
        if (data.id.StartsWith("http") || data.id.StartsWith("www."))
        {
            paused = true;
            SFXPlayer.DownloadAndPlay(data.id, () =>
            {
                paused = false;
            });
        }
        else
        {
            var names = data.id.Split(new char[] { '/' }, System.StringSplitOptions.RemoveEmptyEntries).ToList();
            SFXPlayer.PlayRandom(names);
        }
    }

    void ProcessBGMData(BGMData data)
    {

        if(data.url == "STOP")
        {
            BGMPlayer.Instance.StopBGM();
        }
        else
        {
            paused = true;

            BGMPlayer.Instance.DownloadAndPlay(data.url, () => {
                paused = false;
            });
        }
    }

    void ProcessPlayGameData(PlayGameData data)
    {
        paused = true;
        if(data.number == 1)
        {
            gameRaycaster.StartGame1(() =>
            {
                paused = false;
            });
        }
        else if (data.number == 2)
        {
            gameRaycaster.StartGame2(() =>
            {
                paused = false;
            });
        }
    }


    IEnumerator WaitAndInvoke(float delay, UnityAction action)
    {
        yield return new WaitForSeconds(delay);
        action.Invoke();
    }
}
