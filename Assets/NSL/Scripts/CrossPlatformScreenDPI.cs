﻿using System.Runtime.InteropServices;
using UnityEngine;

public class CrossPlatformScreenDPI : MonoBehaviour
{
#if UNITY_WEBGL
    [DllImport("__Internal")]
    private static extern double GetDPI();
#endif

    private float dpi = 96.0f;
    private bool isInitialized = false;

    private static CrossPlatformScreenDPI _instance;
    public static CrossPlatformScreenDPI Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<CrossPlatformScreenDPI>();

            return _instance;
        }
    }


    private void Initialize()
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        double dpiFromJavascript = GetDPI();
        Debug.Log("dpi from javascript: " + dpiFromJavascript);
        dpi = (float) dpiFromJavascript * 96.0f;
#else
        dpi = Screen.dpi;
#endif
        Debug.Log("resulting dpi: " + dpi);
        isInitialized = true;
    }

    public static float GetScreenDPI()
    {
        if (!Instance.isInitialized)
        {
            Instance.Initialize();
        }
        return Instance.dpi;
    }
}
