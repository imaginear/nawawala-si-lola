﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using TMPro;
using DG.Tweening;

[System.Serializable]
public class ChatSenderData
{
    public ChatSender id;
    public Button button;
    public TextMeshProUGUI lastMessage;
    public TextMeshProUGUI datetime;
    public Texture2D photo;
    public GameObject notif;
}

public class ChatManager : AppBase
{
    private static ChatManager _instance;
    public static ChatManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<ChatManager>();

            return _instance;
        }
    }

    public List<ChatSenderData> senders;

    public List<ScriptableObject> unreadChats;


    public Dictionary<ChatSender, List<ScriptableObject>> chats = new Dictionary<ChatSender, List<ScriptableObject>>();

    [Space(10)]
    public GameObject container;
    public TextMeshProUGUI chatCounterUI;
    public Button backButton, replyButton;
    public TextMeshProUGUI senderLabel;


    [Space(10)]
    public GameObject incomingPrefab;
    public RawImage incomingPrefabProfile;
    public TextMeshProUGUI incomingPrefabMessage;
    public TextMeshProUGUI incomingPrefabDateTime;

    [Space(10)]
    public GameObject outgoingPrefab;
    public TextMeshProUGUI outgoingPrefabMessage;
    public TextMeshProUGUI outgoingPrefabDateTime;


    [Space(10)]
    public GameObject incomingPhotoPrefab;
    public RawImage incomingPhotoPrefabProfile;
    public LoadImage incomingPhotoUrl;
    public TextMeshProUGUI incomingPhotoDateTime;


    [Space(10)]
    public GameObject outgoingPhotoPrefab;
    public LoadImage outgoingPhotoUrl;
    public TextMeshProUGUI outgoingPhotoDateTime;


    Animator anim;

    ChatSender viewingThisSender = ChatSender.NONE;


    public override void Awake()
    {
        base.Awake();

        anim = panel.GetComponent<Animator>();

        backButton.onClick.AddListener(BackButtonPressed);
        replyButton.onClick.AddListener(ReplyButtonPressed);

        foreach(var sender in senders)
        {
            sender.button.onClick.AddListener(() => {
                var senderName = sender.button.transform.Find("Name");
                senderLabel.text = senderName.GetComponent<TextMeshProUGUI>().text;
                OpenChatsFromSender(sender.id);
            });

            chats.Add(sender.id, new List<ScriptableObject>());
             
        }

        OnAppClosed += (id) => {
            if(id == "Chat")
            {
                RemoveRead(viewingThisSender);
            }
        };
    }

    public override void Open()
    {
        base.Open();

        DrawSenderPage();
    }

    public void DrawSenderPage()
    {
        int count = 0;
        foreach (var sender in senders)
        {
            if (chats.ContainsKey(sender.id) && chats[sender.id].Count > 0)
            {
                count++;
                sender.button.gameObject.SetActive(true);

                var lastChat = chats[sender.id][chats[sender.id].Count - 1];

                if (lastChat is ChatDataIn)
                {
                    var c = ((ChatDataIn)lastChat);
                    sender.lastMessage.text = c.message;
                    sender.datetime.text = (c.date == MainDirector.Instance.currentDate) ? c.time : c.date;
                }
                else if (lastChat is ChatDataOut)
                {
                    var c = ((ChatDataOut)lastChat);
                    sender.lastMessage.text = c.answer == 0 ? c.message1 : c.message2;
                    sender.datetime.text = (c.date == MainDirector.Instance.currentDate) ? c.time : c.date;
                }
                else if (lastChat is ChatPhotoDataIn)
                {
                    var c = ((ChatPhotoDataIn)lastChat);
                    sender.lastMessage.text = "Sent a photo";
                    sender.datetime.text = (c.date == MainDirector.Instance.currentDate) ? c.time : c.date;
                }
                else if (lastChat is ChatPhotoDataOut)
                {
                    var c = ((ChatPhotoDataOut)lastChat);
                    sender.lastMessage.text = "Sent a photo";
                    sender.datetime.text = (c.date == MainDirector.Instance.currentDate) ? c.time : c.date;
                }
            }
            else
            {
                sender.button.gameObject.SetActive(false);
            }
        }

        chatCounterUI.text = count + " Chats";
    }

    public void Push(ChatDataIn data)
    {
        SFXPlayer.PlayOneShot("chat alert");

        chats[data.sender].Add(data);
        unreadChats.Add(data);

        var sender = senders.Find(s => s.id == data.sender);
        sender.notif.SetActive(true);


        DrawSenderPage();

        if (viewingThisSender == data.sender)
        {
            SpawnMessage(data);
            //RemoveRead(data.sender);
        }
    }

    public void Push(ChatDataOut data)
    {
        SFXPlayer.PlayOneShot("click");

        chats[data.receiver].Add(data);

        DrawSenderPage();

        if (viewingThisSender == data.receiver)
            SpawnMessage(data);
    }

    public void Push(ChatPhotoDataIn data)
    {
        SFXPlayer.PlayOneShot("chat alert");

        chats[data.sender].Add(data);
        unreadChats.Add(data);

        var sender = senders.Find(s => s.id == data.sender);
        sender.notif.SetActive(true);

        DrawSenderPage();

        if (viewingThisSender == data.sender)
        {
            SpawnMessage(data);
            //RemoveRead(data.sender);
        }
    }

    public void Push(ChatPhotoDataOut data)
    {
        SFXPlayer.PlayOneShot("click");
        chats[data.receiver].Add(data);

        DrawSenderPage();

        if (viewingThisSender == data.receiver)
            SpawnMessage(data);
    }

    public void BackButtonPressed()
    {
        anim.Play("ChatWindowRight");

        RemoveRead(viewingThisSender);

        viewingThisSender = ChatSender.NONE;
    }

    public void ReplyButtonPressed()
    {
        if(MainDirector.Instance.SenderWaitingForReply != viewingThisSender)
        {
            PopUpManager.Instance.Show("Hmmm...\nAno kayang sasabihin ko?", true);
        }
        else
        {
            MessageOptionsPanel.Instance.OpenPanel();
        }
    }

    public void OpenChatsFromSender(ChatSender sender)
    {

        //Clear container
        foreach (Transform child in container.transform)
        {
            if (!child.gameObject.activeSelf || child.name == "Sender Label")//these are prefabs so skip
                continue;

            Destroy(child.gameObject);
        }

        anim.Play("ChatWindowLeft");

        var sorted = chats[sender];
        foreach(var chat in sorted)
        {
            if(chat is ChatDataIn)
            {
                SpawnMessage((ChatDataIn)chat);
            }
            else if(chat is ChatDataOut)
            {
                SpawnMessage((ChatDataOut)chat);
            }
            else if (chat is ChatPhotoDataIn)
            {
                SpawnMessage((ChatPhotoDataIn)chat);
            }
            else if (chat is ChatPhotoDataOut)
            {
                SpawnMessage((ChatPhotoDataOut)chat);
            }
        }

        viewingThisSender = sender;

        RemoveRead(sender);
    }

    void RemoveRead(ChatSender sender)
    {
        //remove notifs
        List<ScriptableObject> readChats = new List<ScriptableObject>();
        foreach (var chat in unreadChats)
        {
            if (chat is ChatDataIn && ((ChatDataIn)chat).sender == sender)
            {
                readChats.Add(chat);
            }
            //else if (chat is ChatDataOut && ((ChatDataOut)chat).receiver == sender)
            //{
            //    readChats.Add(chat);
            //}
            else if (chat is ChatPhotoDataIn && ((ChatPhotoDataIn)chat).sender == sender)
            {
                readChats.Add(chat);
            }
            //else if (chat is ChatPhotoDataOut && ((ChatPhotoDataOut)chat).receiver == sender)
            //{
            //    readChats.Add(chat);
            //}
        }
        foreach (var chat in readChats)
        {
            unreadChats.Remove(chat);
        }

        var senderData = senders.Find(s => s.id == sender);
        if(senderData != null)
            senderData.notif.SetActive(false);

        HomeManager.Instance.SetChatNotif();
    }

    public void ScrollToLowest()
    {
        StartCoroutine(WaitAndInvoke(0.1f, () =>
        {
            var c = container.GetComponent<RectTransform>();
            var p = container.transform.parent.GetComponent<RectTransform>();
            var posy = c.rect.size.y - p.rect.size.y;
            posy = posy < 0 ? 0 : posy;
            //c.anchoredPosition = new Vector2(c.anchoredPosition.x, posy);

            DOTweenModuleUI.DOAnchorPosY(c, posy, 0.2f);
        }));
    }

    public void SpawnMessage(ChatDataIn data)
    {
        var sender = senders.Find(s => s.id == data.sender);

        GameObject prefab = incomingPrefab;
        incomingPrefabMessage.text = data.message;
        incomingPrefabProfile.texture = sender.photo;
        incomingPrefabDateTime.text = MainDirector.Instance.currentDate == data.date ? data.time : data.date;

        var g = Instantiate(prefab, container.transform);
        g.transform.localScale = Vector3.one;
        g.SetActive(true);

        ScrollToLowest();

    }

    public void SpawnMessage(ChatDataOut data)
    {
        var sender = senders.Find(s => s.id == data.receiver);

        GameObject prefab = outgoingPrefab;
        outgoingPrefabMessage.text = data.answer == 0 ? data.message1 : data.message2;
        outgoingPrefabDateTime.text = MainDirector.Instance.currentDate == data.date ? data.time : data.date;


        var g = Instantiate(prefab, container.transform);
        g.transform.localScale = Vector3.one;
        g.SetActive(true);

        ScrollToLowest();

    }

    public void SpawnMessage(ChatPhotoDataIn data)
    {
        var sender = senders.Find(s => s.id == data.sender);

        GameObject prefab = incomingPhotoPrefab;
        incomingPhotoPrefabProfile.texture = sender.photo;
        incomingPhotoUrl.url = data.url;
        incomingPhotoDateTime.text = MainDirector.Instance.currentDate == data.date ? data.time : data.date;


        var g = Instantiate(prefab, container.transform);
        g.transform.localScale = Vector3.one;
        g.SetActive(true);

        ScrollToLowest();


    }

    public void SpawnMessage(ChatPhotoDataOut data)
    {
        var sender = senders.Find(s => s.id == data.receiver);

        GameObject prefab = outgoingPhotoPrefab;
        outgoingPhotoUrl.url = data.url;
        outgoingPhotoDateTime.text = MainDirector.Instance.currentDate == data.date ? data.time : data.date;


        var g = Instantiate(prefab, container.transform);
        g.transform.localScale = Vector3.one;
        g.SetActive(true);

        ScrollToLowest();


    }

    

}
