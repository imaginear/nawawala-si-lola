﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;


[System.Serializable]
public class SFXItem{
	public string name;
	public AudioClip clip;
}

public class SFXPlayer : MonoBehaviour {

	public List<SFXItem> clips;
    public List<AudioSource> sources;

    public Dictionary<string, AudioClip> downloaded = new Dictionary<string, AudioClip>();

	//AudioSource source;

 
	private static SFXPlayer _instance;
	private static SFXPlayer Instance{
		get
		{
			if (_instance == null)
				_instance = GameObject.FindObjectOfType<SFXPlayer>();

			return _instance;
		}
	}

	void Awake(){
        //source = GetComponent<AudioSource>();
        sources = GetComponents<AudioSource>().ToList();
	}

	public static void PlayOneShot(string name){
		PlayOneShot(name, 1, 1);
	}

	public static void PlayOneShot(string name, float volume = 1, float pitch = 1){

        AudioSource source = Instance.sources.Find(s => s.isPlaying == false);
        if (source == null)
            source = Instance.sources[0];

		SFXItem item = Instance.clips.Find(i=>i.name == name);

		if(item != null){
			source.volume = volume;
            source.pitch = pitch;
            source.PlayOneShot(item.clip);
		}
	}

    public static void PlayRandom(List<string> names)
    {
        var i = Random.Range(0, names.Count);
        PlayOneShot(names[i]);
    }

    public static void DownloadAndPlay(string url, UnityAction OnFinished)
    {
        Instance.StartCoroutine(Instance.DownloadAndPlayInternal(url, OnFinished));
    }

    IEnumerator DownloadAndPlayInternal(string url, UnityAction OnFinished)
    {
        AudioClip clip = null;

        if (downloaded.ContainsKey(url))
        {
            clip = downloaded[url];
        }
        else
        {
            using (WWW www = new WWW(url))
            {
                yield return www;
                clip = www.GetAudioClip();
                downloaded.Add(url, clip);
            }
        }
        

        AudioSource source = Instance.sources.Find(s => s.isPlaying == false);
        if (source == null)
            source = Instance.sources[0];

        source.PlayOneShot(clip);

        while (source.isPlaying)
        {
            yield return new WaitForSeconds(0.5f);
        }

        OnFinished.Invoke();
    }


}
