﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

public class GalleryManager : AppBase
{
    private static GalleryManager _instance;
    public static GalleryManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GalleryManager>();

            return _instance;
        }
    }

    public Dictionary<Texture2D, string> photos = new Dictionary<Texture2D, string>();

    public GameObject thumbPrefab;
    public Transform container;
    public RawImage bigPhoto;
    public CanvasGroup bigPhotoPanel;
    public Button closeBigPhotoPanel;

    public delegate void GalleryManagerEvent(string id);
    public static event GalleryManagerEvent OnPhotoViewedInGallery;

    public override void Awake()
    {
        base.Awake();

        bigPhotoPanel.alpha = 0;
        bigPhotoPanel.transform.localScale = 0.8f * Vector3.one;
        closeBigPhotoPanel.onClick.AddListener(HidePhoto);

    }

    public void ShowPhoto(Texture2D photo)
    {
        bigPhoto.texture = photo;

        bigPhotoPanel.gameObject.SetActive(true);


        DOTweenModuleUI.DOFade(bigPhotoPanel, 1, 0.5f);
        bigPhotoPanel.transform.DOScale(1, 0.2f);

        Debug.Log("view " + photos[photo]);

        //send view event here
        if (OnPhotoViewedInGallery != null)
        {
            OnPhotoViewedInGallery(photos[photo]);

        }
    }

    public void HidePhoto()
    {
        DOTweenModuleUI.DOFade(bigPhotoPanel, 0, 0.5f);
        bigPhotoPanel.transform.DOScale(0.8f, 0.5f);

        StartCoroutine(WaitAndInvoke(0.5f, () =>
        {
            bigPhotoPanel.gameObject.SetActive(false);
        }));
    }

    public void AddPhotoToGallery(Texture2D photo, string id)
    {
        photos.Add(photo, id);

        CreateThumbnail(photo);

        Debug.Log("add " + id);
    }

    public void CreateThumbnail(Texture2D photo)
    {
        var i = thumbPrefab.GetComponentInChildren<RawImage>();
        i.texture = photo;

        var a = thumbPrefab.transform.GetChild(0).GetComponent<AspectRatioFitter>();
        a.aspectRatio = (float)photo.width / photo.height;

        var g = Instantiate(thumbPrefab, container);
        g.SetActive(true);

        g.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => {
            ShowPhoto(photo);
        });
    }

    public void DownloadAndAddPhotoToGallery(string url, string id)
    {
        StartCoroutine(DownloadAndAddPhotoToGalleryInternal(url, id));
    }

    IEnumerator DownloadAndAddPhotoToGalleryInternal(string url, string id)
    {
        using (WWW www = new WWW(url))
        {
            yield return www;
            Debug.Log("downloaded " + url);
            AddPhotoToGallery(www.texture, id);
        }
    }
}
