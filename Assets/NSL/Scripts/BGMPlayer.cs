﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class BGMPlayer : MonoBehaviour
{
    private static BGMPlayer _instance;
    public static BGMPlayer Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<BGMPlayer>();

            return _instance;
        }
    }

    AudioSource source;

    public Dictionary<string, AudioClip> downloaded = new Dictionary<string, AudioClip>();

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    public void PlayBGM(AudioClip clip)
    {
        source.clip = clip;
        source.Play();
    }

    public void StopBGM()
    {
        source.Stop();
    }

    public void DownloadAndPlay(string url, UnityAction OnDownloaded)
    {
        Instance.StartCoroutine(Instance.DownloadAndPlayInternal(url, OnDownloaded));
    }

    IEnumerator DownloadAndPlayInternal(string url, UnityAction OnDownloaded)
    {
        AudioClip clip = null;

        if (downloaded.ContainsKey(url))
        {
            clip = downloaded[url];
        }

        else
        {
            using (WWW www = new WWW(url))
            {
                yield return www;
                clip = www.GetAudioClip();
                downloaded.Add(url, clip);
            }
        }

        PlayBGM(clip);

        OnDownloaded.Invoke();

        yield break;
    }
}
