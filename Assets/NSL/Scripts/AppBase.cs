﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using DG.Tweening;

using UnityEngine.Events;

public class AppBase : MonoBehaviour
{
    public GameObject panel;
    private CanvasGroup group;

    protected static AppBase currentApp;

    public string appName;

    float duration = 0.5f;

    public delegate void AppBaseEvent(string id);
    public static event AppBaseEvent OnAppOpened;
    public static event AppBaseEvent OnAppClosed;


    public virtual void Awake()
    {
        group = panel.GetComponent<CanvasGroup>();
        group.gameObject.SetActive(false);
        panel.transform.localScale = 0.8f * Vector3.one;

        //panel.transform.DOScale(0.5f, 1);

        if (appName == "")
            appName = panel.name.Replace(" Screen", "");
    }
    public virtual void Open()
    {
        if(currentApp != null && currentApp != this)
        {
            currentApp.Close();
        }

        currentApp = this;


        group.gameObject.SetActive(true);

        DOTweenModuleUI.DOFade(group, 1, duration);
        panel.transform.DOScale(1, duration);

        StartCoroutine(WaitAndInvoke(duration, () =>
        {
            group.gameObject.SetActive(true);
        }));

        if (OnAppOpened != null)
            OnAppOpened(appName);
    }

    public virtual void Close()
    {
        
        DOTweenModuleUI.DOFade(group, 0, duration);
        panel.transform.DOScale(0.8f, duration);
        StartCoroutine(WaitAndInvoke(duration, () =>
        {
            group.gameObject.SetActive(false);
        }));

        if(OnAppClosed != null)
            OnAppClosed(appName);

    }

    

    public static void CloseCurrent()
    {
        currentApp.Close();
    }

    protected IEnumerator WaitAndInvoke(float delay, UnityAction action)
    {
        yield return new WaitForSeconds(delay);
        action.Invoke();
    }
}
