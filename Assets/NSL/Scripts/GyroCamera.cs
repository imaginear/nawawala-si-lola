﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace XXX
{


    public class GyroCamera : MonoBehaviour
    {

        Quaternion initialRotation, gyroInitialRotation;
        public Transform childCam;

        public Transform world;
        public float mouseSensitivity = 20;

        public bool ignoreUIOccluders = false;


        private static GyroCamera _instance;
        public static GyroCamera Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<GyroCamera>();
                }
                return _instance;
            }
        }

        IEnumerator Start()
        {
            Debug.Log("Screen dpi = " + CrossPlatformScreenDPI.GetScreenDPI());

            if (!SystemInfo.supportsGyroscope)
            {
                initialized = true;
                Debug.Log("device does not support gyro. use mouse instead");
                yield break;
            }

            lastMousePos = Input.mousePosition;

            //initialize gyro
            while (!Input.gyro.enabled)
            {
                Input.gyro.enabled = true;
                Input.gyro.updateInterval = 0.0167f;

                yield return new WaitForEndOfFrame();

                gyroInitialRotation = Quaternion.identity;
                Debug.Log("initializing gyro");
                Input.gyro.enabled = true;
            }

            Debug.Log("gyro initialized: gyro: " + gyroInitialRotation.eulerAngles + ", compass: " + initialRotation.eulerAngles);
            initialized = true;

            yield break;
        }

        // Update is called once per frame

        Vector3 euler;

        bool initialized = false;

        Vector3 lastMousePos;

        public float deceleration = 0.1f;
        public float speed = 0;
        Vector3 pOffsetEul = Vector3.zero;


        void Update()
        {

            

            if (!initialized || lerping)
                return;

            Quaternion offsetRotation = transform.localRotation;
            //Quaternion pOffsetRotation = transform.parent.localRotation;


            if (SystemInfo.supportsGyroscope)
            {
                offsetRotation = Quaternion.Euler(90, -180, 0) * Input.gyro.attitude * Quaternion.Euler(0, 0, 180);
                transform.localRotation = offsetRotation;

            }

            if (EventSystem.current == null || EventSystem.current.currentSelectedGameObject == null || !ignoreUIOccluders)
            {
                if (!CameraManager.Instance.CameraIsOpen() || PopUpManager.Instance.IsOpen())
                    return;


                //Debug.Log("no gyro... drag enabled");

                if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
                {
                    lastMousePos = Input.mousePosition;
                }

                else if (Input.GetMouseButton(0))
                {
                    Vector3 move = -Input.mousePosition + lastMousePos; //pixels/dots
                    lastMousePos = Input.mousePosition;

                    //move = move / Screen.dpi; //convert this to inches
                    move = move / CrossPlatformScreenDPI.GetScreenDPI(); //convert this to inches

                    pOffsetEul = mouseSensitivity * Time.deltaTime * new Vector3(-move.y, -move.x, 0);

                    speed = Vector3.Magnitude(pOffsetEul) / (Time.deltaTime * 60);
                    //lastPOffsetEul = pOffsetEul;
                }

                speed -= deceleration;
                //Debug.Log(speed);
                speed = speed > 5 ? 5 : speed;
                speed = speed < 0 ? 0 : speed;

                if (speed > 0)
                {
                    Vector3 v = pOffsetEul.normalized;


                    world.RotateAround(
                        world.position,
                        Vector3.up,
                        v.y * speed
                    );

                    childCam.RotateAround(
                     world.position,
                     transform.right,
                     v.x * speed
                    );
                }

            }
        }


        bool lerping = false;
        public void ResetAngles()
        {
            StartCoroutine("LerpReset");
        }

        IEnumerator LerpReset()
        {
            if (lerping)
                yield break;

            lerping = true;

            yield return new WaitForEndOfFrame();


            float duration = 0.5f;
            float startTime = Time.time;
            float ease = 0;


            Quaternion startAngleY = world.localRotation;
            Quaternion startAngleX = childCam.localRotation;
            Quaternion targetAngle = Quaternion.identity;//transform.localRotation;

            while (Time.time - startTime < duration)
            {
                ease = Easing.QuadEaseInOut(Time.time - startTime, 0, 1, duration);
                world.localRotation = Quaternion.Slerp(startAngleY, targetAngle, ease);
                childCam.localRotation = Quaternion.Slerp(startAngleX, Quaternion.identity, ease);


                yield return new WaitForEndOfFrame();
            }

            world.localRotation = targetAngle;
            childCam.localRotation = Quaternion.identity;

            lerping = false;
        }

    }

}
