﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;



public class GameRaycaster : MonoBehaviour
{
    public Camera cam;

    int game1Score = 4, game2Score = 3, targetScore = 0, currentScore = 0;
    public GameObject game1, game2;

    UnityAction OnEndGame;

    private static GameRaycaster _instance;
    public static GameRaycaster Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameRaycaster>();

            return _instance;//
        }
    }

    public GameObject staticSphere, gameSphere, world;

    public void Init()
    {
        staticSphere.SetActive(false);
        gameSphere.SetActive(true);
        gameObject.SetActive(true);
        cam.gameObject.SetActive(true);

        world.gameObject.SetActive(true);

        cam.transform.parent.localRotation = Quaternion.identity;
        gameSphere.transform.parent.localRotation = Quaternion.identity;

    }
    public void DeInit()
    {
        staticSphere.SetActive(true);
        gameSphere.SetActive(false);
        gameObject.SetActive(false);
        cam.gameObject.SetActive(false);

        world.gameObject.SetActive(false);


    }

    // Start is called before the first frame update
    public void StartGame1(UnityAction OnEndGame)
    {
        this.OnEndGame = OnEndGame;

        game1.SetActive(true);
        game2.SetActive(false);
        targetScore = game1Score;
        currentScore = 0;

        Init();
    }

    public void StartGame2(UnityAction OnEndGame)
    {
        this.OnEndGame = OnEndGame;

        game1.SetActive(false);
        game2.SetActive(true);
        targetScore = game2Score;
        currentScore = 0;

        Init();

    }

    public void AddScore()
    {
        currentScore++;

        if (currentScore >= targetScore)
        {
            SFXPlayer.PlayOneShot("scream1");

            StartCoroutine(WaitAndInvoke(2, () => {
                PopUpManager.Instance.Show("Magaling! Nahanap ko na lahat!");

            }));

            StartCoroutine(WaitAndInvoke(5, () =>
            {
                OnEndGame.Invoke();

                AppBase.OnAppClosed += ResetOnCameraClosed;
                

            }));
        }
    }

    void ResetOnCameraClosed(string id)
    {
        if(id == "Camera")
        {
            DeInit();
            AppBase.OnAppClosed -= ResetOnCameraClosed;

        
        }
       
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!CameraManager.Instance.CameraIsOpen() || PopUpManager.Instance.IsOpen())
            return;


        var ray = cam.ScreenPointToRay(Input.mousePosition); // Construct a ray from the current mouse coordinates
        var c = Color.white;
        RaycastHit hit;

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                hit.collider.GetComponent<HiddenObject>().Clicked();
                Debug.Log(hit.collider.name);
                c = Color.yellow;

                
            }
        }

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                ray = cam.ScreenPointToRay(touch.position);
                if (Physics.Raycast(ray, out hit))
                {
                    hit.collider.GetComponent<HiddenObject>().Clicked();
                    Debug.Log(hit.collider.name);
                    c = Color.yellow;


                }
            }
        }


        Debug.DrawRay(ray.origin, ray.direction * 10000, c);

    }

    IEnumerator WaitAndInvoke(float delay, UnityAction action)
    {
        yield return new WaitForSeconds(delay);
        action.Invoke();
    }

}
