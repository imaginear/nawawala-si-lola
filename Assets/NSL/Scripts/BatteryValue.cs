﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BatteryValue : MonoBehaviour
{
    public Text batteryVal;

    private static BatteryValue _instance;
    public static BatteryValue Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<BatteryValue>();

            return _instance;//
        }
    }

    private void Start()
    {
        StartCoroutine("IdleInternal");
    }

    // Start is called before the first frame update
    IEnumerator IdleInternal()
    {
        while (true)
        {
            batteryVal.text = (Random.Range(0, 90) + Random.Range(0, 10)) + "%";
            yield return new WaitForSeconds(Random.Range(5, 30));
        }
    }

    IEnumerator LowBattInternal()
    {
        while (true)
        {
            batteryVal.text = Random.Range(0, 1) + "%";
            yield return new WaitForSeconds(Random.Range(1, 5));
        }
    }

    public void LowBatt()
    {
        StopAllCoroutines();
        StartCoroutine("LowBattInternal");
    }
    public void FullBatt()
    {
        StopAllCoroutines();
        StartCoroutine("IdleInternal");
    }

}
