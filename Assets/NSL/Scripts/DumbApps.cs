﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public class DumbApp
{
    public string id;
    public Texture2D front;
}

public class DumbApps : AppBase
{
    private static DumbApps _instance;
    public static DumbApps Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<DumbApps>();

            return _instance;//
        }
    }

    public List<DumbApp> apps;
    public RawImage backdrop;

    public void Open(string id)
    {
        var app = apps.Find(a => a.id == id);
        backdrop.texture = app.front;

        appName = app.id;

        base.Open();
    }


    
}
