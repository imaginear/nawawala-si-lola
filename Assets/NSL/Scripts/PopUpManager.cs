﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using UnityEngine.Events;

public class PopUpManager : MonoBehaviour
{
    private static PopUpManager _instance;
    public static PopUpManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<PopUpManager>();

            return _instance;//
        }
    }

    public Button closeButton;
    public Button bgButton;
    public GameObject panel;
    public TextMeshProUGUI message;

    UnityAction OnClosed;

    public void Awake()
    {
        closeButton.onClick.AddListener(Hide);
        //bgButton.onClick.AddListener(Hide);

    }

    public bool IsOpen()
    {
        return panel.activeSelf;
    }

    public void Show( string message, bool mute = false)
    {
        this.message.text = message;
        panel.SetActive(true);

        if (!mute)
            SFXPlayer.PlayOneShot("clue alert");
        else
            SFXPlayer.PlayOneShot("low batt");
    }

    public void Hide()
    {
        panel.SetActive(false);

        if(OnClosed != null)
        {
            OnClosed.Invoke();
            OnClosed = null;
        }
    }

    public void Show(string message, UnityAction OnClosed)
    {
        this.OnClosed = OnClosed;
        Show(message);
        
    }

}
