﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

using System;
using System.Net;
using System.IO;

using System.Linq;

public class DataBuilder
{
    [MenuItem("Game/Fetch Data")]
    static void FetchData()
    {

        WebClient client = new WebClient();

        var path = "https://docs.google.com/spreadsheets/u/0/d/1gbZ8GUhWUxeaEJbh20xvL_md4TAPRIdKhFczlef0_rs/export?format=csv&id=1gbZ8GUhWUxeaEJbh20xvL_md4TAPRIdKhFczlef0_rs&gid=0";

        Debug.Log("Fetching from " + path);
        EditorUtility.DisplayProgressBar("Fetching data...", "Please Wait...", 0);

        Stream data = client.OpenRead(path);
        StreamReader reader = new StreamReader(data);
        string s = reader.ReadToEnd();
        data.Close();
        reader.Close();

        ClearData();
        if(!Directory.Exists(Application.dataPath + "/NSL/Data"))
        {
            Directory.CreateDirectory(Application.dataPath + "/NSL/Data");
        }


        EditorUtility.DisplayProgressBar("Building data...", "Please Wait...", 0);
        var lines = s.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
        int ctr = 1;


        foreach(var line in lines)
        {
            EditorUtility.DisplayProgressBar("Building data...", ctr + " of " + lines.Length, (float)ctr/lines.Length);

            Debug.Log(line);
            Build(line, ctr.ToString("000"));
            ctr++;
        }

        EditorUtility.ClearProgressBar();

    }

    

    static void Build(string line, string ctr)
    {

        line = line.Replace("\r\n", "").Replace("\n", "").Replace("\r", "");


        List<string> fields = line.Split(new char[] { ','}, StringSplitOptions.RemoveEmptyEntries ).ToList();
        if(fields[0] == "CHAT_IN")
        {
            var a = ScriptableObject.CreateInstance<ChatDataIn>();
            a.sender = (ChatSender)Enum.Parse(typeof(ChatSender), fields[1]);
            a.message = fields[2];
            a.time = fields[3];
            a.date = fields[4];

            AssetDatabase.CreateAsset(a, "Assets/NSL/Data/" + ctr + "-" + fields[0] + ".asset");
            AssetDatabase.SaveAssets();

            MainDirector.Instance.events.Add(a);
        }
        else if (fields[0] == "CHAT_OUT")
        {
            var a = ScriptableObject.CreateInstance<ChatDataOut>();
            a.receiver = (ChatSender)Enum.Parse(typeof(ChatSender), fields[1]);
            a.message1 = fields[2];
            a.message2 = fields[3];
            a.time = fields[4];
            a.date = fields[5];

            AssetDatabase.CreateAsset(a, "Assets/NSL/Data/" + ctr + "-" + fields[0] + ".asset");
            AssetDatabase.SaveAssets();

            MainDirector.Instance.events.Add(a);
        }
        else if (fields[0] == "CHAT_PHOTO_IN")
        {
            var a = ScriptableObject.CreateInstance<ChatPhotoDataIn>();
            a.sender = (ChatSender)Enum.Parse(typeof(ChatSender), fields[1]);
            a.url = fields[2];
            a.time = fields[3];
            a.date = fields[4];

            AssetDatabase.CreateAsset(a, "Assets/NSL/Data/" + ctr + "-" + fields[0] + ".asset");
            AssetDatabase.SaveAssets();

            MainDirector.Instance.events.Add(a);
        }
        else if (fields[0] == "CHAT_PHOTO_OUT")
        {
            var a = ScriptableObject.CreateInstance<ChatPhotoDataOut>();
            a.receiver = (ChatSender)Enum.Parse(typeof(ChatSender), fields[1]);
            a.url = fields[2];
            a.time = fields[3];
            a.date = fields[4];

            AssetDatabase.CreateAsset(a, "Assets/NSL/Data/" + ctr + "-" + fields[0] + ".asset");
            AssetDatabase.SaveAssets();

            MainDirector.Instance.events.Add(a);
        }

        else if (fields[0] == "LOW_BATT")
        {
            var a = ScriptableObject.CreateInstance<LowBattData>();
            a.charged = fields[1] == "CHARGED";
            try
            {
                a.message = fields[2];
            }
            catch { }

            AssetDatabase.CreateAsset(a, "Assets/NSL/Data/" + ctr + "-" + fields[0] + ".asset");
            AssetDatabase.SaveAssets();

            MainDirector.Instance.events.Add(a);
        }

        else if (fields[0] == "POPUP")
        {
            var a = ScriptableObject.CreateInstance<PopupData>();
            a.message = fields[1];

            AssetDatabase.CreateAsset(a, "Assets/NSL/Data/" + ctr + "-" + fields[0] + ".asset");
            AssetDatabase.SaveAssets();

            MainDirector.Instance.events.Add(a);
        }

        else if (fields[0] == "DELAY")
        {
            var a = ScriptableObject.CreateInstance<DelayData>();
            a.delay = (float)System.Convert.ToDouble(fields[1]);

            AssetDatabase.CreateAsset(a, "Assets/NSL/Data/" + ctr + "-" + fields[0] + ".asset");
            AssetDatabase.SaveAssets();

            MainDirector.Instance.events.Add(a);
        }

        else if (fields[0] == "ADD_PHOTO_TO_GALLERY")
        {
            var a = ScriptableObject.CreateInstance<AddPhotoToGalleryData>();
            a.id = fields[1];
            a.url = fields[2];

            AssetDatabase.CreateAsset(a, "Assets/NSL/Data/" + ctr + "-" + fields[0] + ".asset");
            AssetDatabase.SaveAssets();

            MainDirector.Instance.events.Add(a);
        }

        else if (fields[0] == "WAIT_FOR_PHOTO_VIEW")
        {
            var a = ScriptableObject.CreateInstance<WaitPhotoViewData>();
            a.id = fields[1];

            AssetDatabase.CreateAsset(a, "Assets/NSL/Data/" + ctr + "-" + fields[0] + ".asset");
            AssetDatabase.SaveAssets();

            MainDirector.Instance.events.Add(a);
        }




        else if (fields[0] == "SET_WALL_BG")
        {
            var a = ScriptableObject.CreateInstance<SetWallBGData>();
            a.url = fields[1];

            AssetDatabase.CreateAsset(a, "Assets/NSL/Data/" + ctr + "-" + fields[0] + ".asset");
            AssetDatabase.SaveAssets();

            MainDirector.Instance.events.Add(a);
        }

        else if (fields[0] == "SET_CAM_BG")
        {
            var a = ScriptableObject.CreateInstance<SetCamBGData>();
            a.url = fields[1];

            AssetDatabase.CreateAsset(a, "Assets/NSL/Data/" + ctr + "-" + fields[0] + ".asset");
            AssetDatabase.SaveAssets();

            MainDirector.Instance.events.Add(a);
        }

        else if(fields[0] == "WAIT_APP_OPEN")
        {
            var a = ScriptableObject.CreateInstance<WaitAppOpen>();
            a.appName = fields[1];

            AssetDatabase.CreateAsset(a, "Assets/NSL/Data/" + ctr + "-" + fields[0] + ".asset");
            AssetDatabase.SaveAssets();

            MainDirector.Instance.events.Add(a);
        }

        else if (fields[0] == "PLAY_AUDIO")
        {
            var a = ScriptableObject.CreateInstance<PlayAudioData>();
            a.id = fields[1];

            AssetDatabase.CreateAsset(a, "Assets/NSL/Data/" + ctr + "-" + fields[0] + ".asset");
            AssetDatabase.SaveAssets();

            MainDirector.Instance.events.Add(a);
        }

        else if (fields[0] == "BGM")
        {
            var a = ScriptableObject.CreateInstance<BGMData>();
            a.url = fields[1];

            AssetDatabase.CreateAsset(a, "Assets/NSL/Data/" + ctr + "-" + fields[0] + ".asset");
            AssetDatabase.SaveAssets();

            MainDirector.Instance.events.Add(a);
        }

        else if (fields[0] == "PLAY_GAME")
        {
            var a = ScriptableObject.CreateInstance<PlayGameData>();
            a.number = System.Convert.ToInt16(fields[1]);

            AssetDatabase.CreateAsset(a, "Assets/NSL/Data/" + ctr + "-" + fields[0] + ".asset");
            AssetDatabase.SaveAssets();

            MainDirector.Instance.events.Add(a);
        }

    }

    static void ClearData()
    {
        GameObject.FindObjectOfType<MainDirector>().events.Clear();

        var datapath = Application.dataPath + "/NSL/Data";

        if (Directory.Exists(datapath))
        {
            Debug.Log("Deleting " + datapath);
            DeleteDirectory(datapath);
        }
    }

    public static void DeleteDirectory(string target_dir)
    {
        string[] files = Directory.GetFiles(target_dir);
        string[] dirs = Directory.GetDirectories(target_dir);

        foreach (string file in files)
        {
            File.SetAttributes(file, FileAttributes.Normal);
            File.Delete(file);
        }

        foreach (string dir in dirs)
        {
            DeleteDirectory(dir);
        }

        Directory.Delete(target_dir, false);
    }
}
