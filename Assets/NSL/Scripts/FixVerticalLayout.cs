﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(VerticalLayoutGroup))]
public class FixVerticalLayout : MonoBehaviour
{
    public int priority = 0;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();


        for (int i = 0; i < priority; i++)
        {
            yield return new WaitForEndOfFrame();
        }

        //fix
        var v = GetComponent<VerticalLayoutGroup>();
        float height = v.padding.top + v.padding.bottom;
        for(int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i).GetComponent<RectTransform>();
            if (!child.gameObject.activeSelf)
                continue;
            height += child.sizeDelta.y;
            height += v.spacing;
            //Debug.Log(name + ": " + child.sizeDelta.y);

            var l = child.gameObject.AddComponent<LayoutElement>();
            l.minHeight = child.sizeDelta.y;
        }

        height -= v.spacing;

        //Debug.Log(name + ": total = " + height);
    }

}
