﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using TMPro;
public class MessageOptionsPanel : MonoBehaviour
{
    private static MessageOptionsPanel _instance;
    public static MessageOptionsPanel Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<MessageOptionsPanel>();

            return _instance;
        }
    }

    public GameObject panel;
    [Space(10)]
    public Button reply1;
    public TextMeshProUGUI reply1Text;

    [Space(10)]
    public Button reply2;
    public TextMeshProUGUI reply2Text;

    public void SetPanel(string text1, string text2, UnityAction callback1, UnityAction callback2)
    {

        reply1Text.text = text1;
        reply2Text.text = text2;

        reply1.onClick.RemoveAllListeners();
        reply2.onClick.RemoveAllListeners();

        reply1.onClick.AddListener(callback1);
        reply2.onClick.AddListener(callback2);

    }

    public void OpenPanel()
    {
        panel.SetActive(true);
    }

    public void ClosePanel()
    {
        panel.SetActive(false);
    }
}
