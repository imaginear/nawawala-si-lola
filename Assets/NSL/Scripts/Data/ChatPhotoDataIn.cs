﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ChatPhotoData - In", menuName = "Data/Chat Photo Data - Incoming", order = 1)]
public class ChatPhotoDataIn : ScriptableObject
{
    public ChatSender sender;
    [TextArea(2, 5)]
    public string url;
    public string date;
    public string time;
}
