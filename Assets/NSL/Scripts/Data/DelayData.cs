﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DelayData", menuName = "Data/Delay Data", order = 1)]
public class DelayData : ScriptableObject
{
    public float delay;
}
