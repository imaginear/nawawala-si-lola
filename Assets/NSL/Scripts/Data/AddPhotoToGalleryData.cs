﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AddPhotoToGalleryData", menuName = "Data/Add Photo To Gallery Data", order = 1)]
public class AddPhotoToGalleryData : ScriptableObject
{
    public string url;
    public string id;
}
