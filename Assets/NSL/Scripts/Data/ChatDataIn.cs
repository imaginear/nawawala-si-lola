﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ChatData - In", menuName = "Data/Chat Data - Incoming", order = 1)]
public class ChatDataIn : ScriptableObject
{
    public ChatSender sender;
    [TextArea(2, 5)]
    public string message;
    public string date;
    public string time;
}
