﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ChatData - Out", menuName = "Data/Chat Data - Outgoing", order = 1)]
public class ChatDataOut : ScriptableObject
{
    public ChatSender receiver;
    [TextArea(2, 5)]
    public string message1;
    [TextArea(2, 5)]
    public string message2;
    public int answer = -1;

    public string date;
    public string time;
}
