﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SetCamBGData", menuName = "Data/Set Cam BG Data", order = 1)]
public class SetCamBGData : ScriptableObject
{
    public string url;
}
