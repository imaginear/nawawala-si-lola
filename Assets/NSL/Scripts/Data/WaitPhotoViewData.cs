﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WaitPhotoViewData", menuName = "Data/Wait Photo View Data", order = 1)]
public class WaitPhotoViewData : ScriptableObject
{
    public string id;
}
