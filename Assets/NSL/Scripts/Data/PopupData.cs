﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PopupData", menuName = "Data/Popup Data", order = 1)]
public class PopupData : ScriptableObject
{
    public string message;
}
