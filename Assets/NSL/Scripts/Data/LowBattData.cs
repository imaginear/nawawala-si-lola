﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LowBattData", menuName = "Data/LowBatt Data", order = 1)]
public class LowBattData : ScriptableObject
{
    public bool charged = false;
    public string message;
}
