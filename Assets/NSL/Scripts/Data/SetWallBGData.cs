﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SetWallBGData", menuName = "Data/Set Wall BG Data", order = 1)]
public class SetWallBGData : ScriptableObject
{
    public string url;
}
