﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ChatPhotoData - Out", menuName = "Data/Chat Photo Data - Outgoing", order = 1)]
public class ChatPhotoDataOut : ScriptableObject
{
    public ChatSender receiver;
    [TextArea(2, 5)]
    public string url;
    public string date;
    public string time;
}