﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;
using TMPro;

public class CameraManager : AppBase
{
    private static CameraManager _instance;
    public static CameraManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<CameraManager>();

            return _instance;//
        }
    }

    public Camera cam;
    public GameObject World;
    public RawImage image;
    public Button buttonCapture;

    public Renderer StaticSphere;

    protected static Dictionary<string, Texture2D> downloaded = new Dictionary<string, Texture2D>();



    public bool CameraIsOpen() {
        return panel.activeInHierarchy;
    }

    public override void Awake()
    {
        base.Awake();
        buttonCapture.onClick.AddListener(Capture);
    }

    public override void Open()
    {
        base.Open();
        World.SetActive(true);

        World.transform.localRotation = Quaternion.identity;
        cam.transform.parent.localRotation = Quaternion.identity;

    }

    public void Capture()
    {
        SFXPlayer.PlayOneShot("cam click");
    }

    public void SetCamBG(Texture2D bg)
    {
        StaticSphere.material.mainTexture = bg;
    }

    public void DownloadAndSetCamBG(string url)
    {
        StartCoroutine(DownloadAndSetCamBGInternal(url));
    }
    IEnumerator DownloadAndSetCamBGInternal(string url)
    {
        if (downloaded.ContainsKey(url))
        {
            SetCamBG(downloaded[url]);
            yield break;
        }

        using (WWW www = new WWW(url))
        {
            yield return www;
            SetCamBG(www.texture);
            downloaded.Add(url, www.texture);
        }
    }
}
