﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using DG.Tweening;
public class LowBattManager : MonoBehaviour
{
    private static LowBattManager _instance;

    public static LowBattManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<LowBattManager>();

            return _instance;//
        }
    }

    public GameObject panel;
    public Image black;

    public void Show(string message, UnityAction OnFinished)
    {
        panel.SetActive(true);
        StartCoroutine(WaitAndInvoke(3, () =>
        {
            DOTweenModuleUI.DOColor(black, new Color(0, 0, 0, 1), 1);
        }));

        StartCoroutine(WaitAndInvoke(4, () =>
        {
            PopUpManager.Instance.Show(message, OnFinished);
        }));

        SFXPlayer.PlayOneShot("low batt");
    }

    public void Hide(UnityAction OnFinished)
    {
        DOTweenModuleUI.DOColor(black, new Color(0, 0, 0, 0), 1);
        panel.SetActive(false);

        AppBase.CloseCurrent();

        StartCoroutine(WaitAndInvoke(1, () =>
        {
            OnFinished.Invoke();
        }));
    }

    IEnumerator WaitAndInvoke(float delay, UnityAction action)
    {
        yield return new WaitForSeconds(delay);
        action.Invoke();
    }

}
