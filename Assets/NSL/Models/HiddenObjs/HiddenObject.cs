﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


using DG.Tweening;

[RequireComponent(typeof(MeshCollider))]
public class HiddenObject : MonoBehaviour
{
    public bool isCorrect;

    [TextArea(2,5)]
    public string objectInfo;

    public void Clicked()
    {

        if (isCorrect)
        {
            transform.DOScale(1.8f, 0.5f);
            transform.DOLocalMoveY(0.1f, 0.5f);

            StartCoroutine(WaitAndInvoke(0.5f, () =>
            {
                gameObject.SetActive(false);
                PopUpManager.Instance.Show(objectInfo, isCorrect ? false : true);

                GameRaycaster.Instance.AddScore();

            }));
        }
        else
        {
            PopUpManager.Instance.Show(objectInfo, isCorrect ? false : true);
        }
    }

    IEnumerator WaitAndInvoke(float delay, UnityAction action)
    {
        yield return new WaitForSeconds(delay);
        action.Invoke();
    }

}
